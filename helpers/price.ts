export const getPrettyPrice = (price: number): string => {
  let newString = ''
  price.toString().split('').reverse().forEach((i, index) => {
    if (index % 3 === 0) newString += ' '
    newString += i
  })
  return newString.split('').reverse().join('')
}
