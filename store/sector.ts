import { defineStore } from "pinia"

type Sector = string | null

interface SectorInfo {
  name: string
  title: string
  price: number
  people: number
  square: number
}

interface SectorState {
	sector: Sector
  showMessage: boolean
  messageText: string
  sectors: SectorInfo[]
}

export const useSector = defineStore('sector', {
  state: (): SectorState => ({
    sector: null,
    showMessage: false,
    messageText: '',
    sectors: [],
  }),
  actions: {
    setSector(sector: Sector) {
      this.sector = sector;

      if (sector) {
        this.messageText = `Выбран сектор - ${sector}`
        this.showMessage = true
        setTimeout(() => { this.showMessage = false }, 1000)
      }
    },
    async loadSectors() {
      const { data } = await useFetch('/api/sectors')
      this.sectors = data.value.sectors
    },
  }
})
