export default(req, res) => ({
  status: 'ok',
  sectors: [
    { name: 'a', title: 'Офис A',  price: 60000, people: 4, square: 14 },
    { name: 'b', title: 'Офис B',  price: 66040, people: 5, square: 17 },
    { name: 'c', title: 'Офис C',  price: 70700, people: 6, square: 12 },
    { name: 'd', title: 'Офис D',  price: 60000, people: 4, square: 11 },
    { name: 'e', title: 'Офис E',  price: 60000, people: 2, square: 18 },
    { name: 'f', title: 'Офис F',  price: 40000, people: 1, square: 19 },
    { name: 'g', title: 'Офис G',  price: 26000, people: 8, square: 13 },
    { name: 'h', title: 'Офис H',  price: 66000, people: 9, square: 12 },
    { name: 'i', title: 'Офис I',  price: 90000, people: 6, square: 11 },
    { name: 'j', title: 'Офис J',  price: 10000, people: 4, square: 11 },
  ],
})
